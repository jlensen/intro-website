let container = document.querySelector(".images");
let button = document.querySelector(".game-wrap button");
let tries = 3;
let text = document.querySelector(".text p");
let triesText = document.querySelector(".text .tries");

// Adds images
function add() {
    for (let i = 1; i < 10; i++) {
        let ref = "assets/img/game/" + i + ".jpg";
        let img = document.createElement("img");
        img.setAttribute("src", ref);
        container.appendChild(img);
    }
}
add();

// set main image
let imgs = document.querySelectorAll(".images img");
let main = document.querySelector(".exampleimg");
let rand = Math.round(Math.random() * 8);
main.appendChild(imgs[rand].cloneNode(true));

// reselect main image
function shuffle() {
    reset();
    add();
    triesText.innerHTML = "Tries: 4"
    tries = 3;
    let rand = Math.round(Math.random() * 8) + 1;
    let img = document.querySelector(".exampleimg img");
    img.setAttribute("src", "assets/img/game/" + rand + ".jpg");
    text.innerHTML = "Guess the image!";

}

// remove images
function reset() {
    while (container.firstChild) {
        container.removeChild(container.firstChild);
    }
}


// check result
function clicked(event) {
    triesText.innerHTML = "Tries: " + tries;
    if (tries == 0) {
        text.innerHTML = "Je beurten zijn voorbij, klik op reset!";
        reset();
        return;
    }
    let element = event.target;
    if (event.target.nodeName === "IMG") {
        tries--;
        let mainimg = document.querySelector(".exampleimg img");

        if (element.getAttribute("src") === mainimg.getAttribute("src")) {
            text.innerHTML = "You did it!";
            reset();
        } else {
            text.innerHTML = "You failed!";
        }
    }
}

container.addEventListener("click", function(event) { clicked(event)});
button.addEventListener("click", shuffle);




