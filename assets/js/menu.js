window.addEventListener("load", init);

function init() {
    let button = document.querySelector(".button");
    button.addEventListener("click", togglemenu);
}

function togglemenu() {
    var mitem = document.getElementById("menu-items");
    if (mitem.style.display === "block") {
        mitem.style.display = "none";
    } else {
        mitem.style.display = "block";
    }
}