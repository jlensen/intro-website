window.addEventListener("load", init);

function init() {
    let images = document.querySelector(".img-container");
    images.addEventListener("click", function(e) {getClick(e)});
    window.addEventListener("click", function(e) {close(e)});
}

function getClick(event) {
    let modal = document.getElementsByClassName("modal");
    let target = event.target.parentNode;
    let num = target.getAttribute("data-modal");
    modal[num].style.display = "block";
}

function close(event) {
    let modal = document.getElementsByClassName("modal");
    if (event.target === modal[event.target.getAttribute("data-modal")]) {
        modal[event.target.getAttribute("data-modal")].style.display = "none";

    }

    if (event.target.nodeName === "I") {
        event.target.parentNode.parentNode.parentNode.style.display = "none";
    }
}